function event = ft_readmarkerfile(cfg)


def.markerfile = 'MarkerFile.mrk';
def.hdr        = [];
def.dataset    = '';

cfg = setdef(cfg,def);

if isempty(cfg.hdr)
    if isempty(cfg.dataset)
        error('Need sample information. Please provide cfg.hdr or cfg.dataset.')
    else
        cfg.hdr = ft_read_header(cfg.dataset);
    end
elseif ~isstruct(cfg.hdr)
    error('cfg.hdr should be a header structure.')
end

mrk = readmarkerfile(cfg.markerfile);
event = struct([]);
for i=1:mrk.number_markers
    for j=1:mrk.number_samples(i)
        % determine the location of the marker, expressed in samples
        trialnum = mrk.trial_times{i}(j,1);
        synctime = mrk.trial_times{i}(j,2);
        begsample = (trialnum-1)*cfg.hdr.nSamples + 1;    % of the trial, relative to the start of the datafile
        endsample = (trialnum  )*cfg.hdr.nSamples;        % of the trial, relative to the start of the datafile
        offset    = round(synctime*cfg.hdr.Fs);           % this is the offset (in samples) relative to time t=0 for this trial
        offset    = offset + cfg.hdr.nSamplesPre;         % and time t=0 corrsponds with the nSamplesPre'th sample
        % store this marker as an event
        event(end+1).type    = mrk.marker_names{i};
        event(end ).value    = [];
        event(end ).sample   = begsample + offset;
        event(end ).duration = 0;
        event(end ).offset   = offset;
    end
end
types = {event.type};
if numel(types) ~= numel(event)
    error('All markers should be named')
end
evtlist = flister('(?<n>\w+)__(?<ss>START|END)__','list',types,'sortfields',{});
[evtlist.idx] = rep2struct(1:numel(evtlist));
names = unique({evtlist.n});
nuevent = [];
for i = 1:numel(names)
    si = evtlist(strcmp({evtlist.n},names{i}));
    starts = si(strcmp({si.ss},'START'));
    stops  = si(strcmp({si.ss},'END'));
    if numel(starts) ~= numel(stops)
        error('marker START - END inconsistency')
    end
    for j = 1:numel(starts)
        nuevent(end+1).type = starts(j).n;
        nuevent(end).value = [];
        nuevent(end).sample = event(starts(j).idx).sample;
        nuevent(end).duration = event(stops(j).idx).sample - nuevent(end).sample;
        nuevent(end).offset = event(starts(j).idx).offset;
    end
end
event = nuevent;
return


function [marker] = readmarkerfile(name)

% Read the MarkerFile.mrk file in a CTF dataset.
%
% Use as
%   marker = readmarkerfile(name)
%
% Creates a marker structure which contains number_markers,
% number_samples, marker_names, and trial_times.

% Contributed by Tom Holroyd by email on 21 May 2005

if ~exist(name, 'file')
  error('%s not found', name);
end

f = fopen(name, 'rt');
markfile = {};
while true
  l = fgetl(f);
  if ~ischar(l)
    break
  end
  markfile{end + 1} = l;
end
fclose(f);

% Only one of these.
i = strmatch('NUMBER OF MARKERS:', markfile, 'exact') + 1;
nmarkers = str2num(markfile{i});

% Get all the marker names.
i = strmatch('NAME:', markfile, 'exact') + 1;
names = markfile(i);

% Get the number of samples for each.
i = strmatch('NUMBER OF SAMPLES:', markfile, 'exact') + 1;
nsamples = str2num(char(markfile(i)));

% for i = 1:length(nsamples)
%   if nsamples(i) == 0
%     warning('marker %s in %s has zero samples', names{i}, name);
%   end
% end

% Get the samples.  Each is trial and time in seconds.
j = strmatch('LIST OF SAMPLES:', markfile, 'exact') + 2;
for i = 1:nmarkers
  marks{i} = str2num(char(markfile(j(i):j(i) + nsamples(i))));
  
  % Convert from index origin 0 to 1
  if nsamples(i) ~= 0
    marks{i}(:, 1) = marks{i}(:, 1) + 1;
  end
end

marker = struct('number_markers', {nmarkers}, 'number_samples', {nsamples}, ...
  'marker_names', {names}, 'trial_times', {marks});

