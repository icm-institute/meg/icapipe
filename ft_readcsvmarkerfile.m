function event = ft_readcsvmarkerfile(cfg)


def.markerfile = 'MarkerFile.csv';
def.hdr        = [];
def.dataset    = '';

cfg = setdef(cfg,def);

if isempty(cfg.hdr)
    if isempty(cfg.dataset)
        error('Need sample information. Please provide cfg.hdr or cfg.dataset.')
    else
        cfg.hdr = ft_read_header(cfg.dataset);
    end
elseif ~isstruct(cfg.hdr)
    error('cfg.hdr should be a header structure.')
end

mrk = readtable(cfg.markerfile);
mrk.Properties.VariableNames = {'type','sample','duration'};
event = table2struct(mrk);
[event.sample] = rep2struct([event.sample] * cfg.hdr.Fs);
[event.duration] = rep2struct([event.duration] * cfg.hdr.Fs);
[event.value] = rep2struct([]);
[event.offset] = rep2struct(0);
