# Using ICA to reject blinks, cardiac, and other artifacts

Note this repo is stored in `/servernas/usr_cenir/matlab/maxtools` along with other usefull tools.

## Rationale

- Massive artifacts like blinks and cardio account for a large portion of variance in MEG data

- These artifacts recurr in the data and are usually stable in topography

- ICA is a choice method to attempt removing these artifacts.

## Rationale: ICA

- ICA transforms the data into a sum of "sources" or "independent components"(IC) (i.e. weighted sum of channels) that are maximally independent from each other

- These ICs can be easily subtracted from the data

- ICA decompositions often capture the cardiac and blink artifacts in one IC.

## Computing ICA on data

 ```matlab
 copyfile('/servernas/usr_cenir/matlab/maxtools/icapipe/pipe_icarej.m','/where/you/want/it/mypipe.m')
 edit mypipe.m % follow instructions in this script
 ```


This will
- perform ICA
- select ICs that correlate with a given channel (choose ECG, EOGs...)
- other methods are available. See `edit SASICA/eeg_SASICA.m`
- explore what subtracting the selected ICs will do
- subtract ICs and save

## Q&A

- Including EOGs and ECGs in the ICA?

Yes, this improves separation. It "guides" the ICA and will push the components "up" in the plots (because they explain more variance).

- Removing bad portions of signal?

This is always a good idea for non reproducible artifacts (movement, swallow...)

- Downsample the data

Will improve speed. We have usually too much data. Resampling at 100 Hz should be ok.

- How many components?

A bit of trial and error here. Asking for too many will prevent convergence (and is slow), too few will see a poor separation (but is quick). 2/3 of the rank of the data is in my experience a rough guide.

- Questions? maximilien.chaumon@icm-institute.org
