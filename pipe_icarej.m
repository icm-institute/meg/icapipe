%% pipeline de traitement pour
% 1) décomposition ICA
% 2) sélection des composantes corrélant fortement avec des cannaux
% d'intéret (e.g. cardio, EOG...)
% 3) Exploration des composantes et de l'effet de leur suppression.
% 4) Soustraction des composantes et sauvegarde au format .fif.

%% init code
addpath('/network/lustre/iss01/cenir/analyse/meeg/00_max/share')
add_ miscMatlab
add_ miscMatlab/fieldtrip
add_ fieldtrip
ft_defaults
add_ icapipe
add_ icapipe/SASICA


%% read and compute ICA
file = '/network/lustre/iss01/cenir/analyse/meeg/00_max/share/example_data.fif'; % choose your MEG file

cfg  = [];
cfg.dataset = file;
cfg.channel = {'MEG' 'BIO*'}; % choose channels (include EOGs/ECGs...)
cfg.excludebad = 'yes'; % whether or not to exclude bad portions of signal
%                         note you should mark bad segments of data using
%                         muse (in a terminal: muse /your/file.fif)

[data, comp ] = do_ica(cfg);
if any(~isreal(comp.trial{1}))
    error('Seems like the ICA went wrong... check your data');
end

%% Use SASICA to select components correlated with some channels
cfg  = [];
cfg.chancorr.enable = 1;    % enable the 'channel correlation' method of SASICA
cfg.chancorr.corthresh = .5;% correlation threshold is 0.5
cfg.chancorr.channames = {'BIO001','BIO002'};% the channels that should be used
%                       Any component that correlates with any of these
%                       channels above threshold will be returned in
%                       comps2del below.
% Other methods are available. Type doc eeg_SASICA in the command window for
% more information.

% SASICA opens a series of IC topographies. The selected ones have a red
% button below. Click on the button to see IC properties. Click "show
% subtraction" at the bottom of the window to show effect of subtraction on
% the time courses.
comps2del = ft_SASICA_neuromag(cfg,comp,data);

%% Subtract components
cfg                           = [];
cfg.component                 = comps2del ;
data_corrected                = ft_rejectcomponent(cfg, comp, data );

%% write results
[p,n,~] = fileparts(file) ;

component_filename = [p filesep n '_ica_comp.mat'];
save(component_filename,'comp');

output_filename = [ p filesep n '_ica_cor.fif' ];
fieldtrip2fiff( [ output_filename ], data_corrected );
