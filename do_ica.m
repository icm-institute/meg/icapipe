function [ data, comp ] = do_ica( cfg, data )


% do_ica reads MEG and/or EEG data and computes an ICA decomposition using
% the runica method of EEGLAB, ignoring bad segments of data according to a
% .mrk markerfile. It returns the original data (filtered, but bad segments
% NOT discarded), and the ICA decomposition.
%
% Use as
%   [data, comp] = do_ica(cfg)
%   [data, comp] = do_ica(cfg, data)
%
% The cfg structure should contain the following fields:
%
%     cfg.dataset     = string with the input filename (if data is not
%                       provided)
%     cfg.ncomp       = number of components to estimate (default = 'auto',
%                       rank of the data matrix)
%     cfg.channel     = channels to include in the ICA decomposition
%                       (default = {'MEG' 'BIO*'})
%     cfg.hpfilter    = whether to high pass filter the data prior to
%                       running the ICA (default = 'yes')
%     cfg.hpfreq      = high pass frequency (default = 0.1 Hz)
%     cfg.hpfiltord   = filter order (default = 3)
%     cfg.doresample  = whether or not to resample the data prior to ICA
%                       decomposition (default = 'yes')
%     cfg.doresamplefs = resampling frequency in Hz (default = 100)
%     cfg.excludebad  = whether to exclude bad segments of data or not
%                       (default = 'no'). If yes, the .mrk filename should
%                       be the same as cfg.dataset except for the
%                       extension.
%     cfg.excludename = name of the marker to exclude (default = 'BAD')
% 
% Additional fields of the cfg structure are passed to ft_preprocessing
% 
%

def = [];
def.channel     = {'MEG' 'BIO*'};
def.hpfilter    = 'yes';
def.hpfreq      = .1;
def.hpfiltord   = 3;
def.excludebad  = 'no';
def.excludename = 'BAD';
def.doresample  = 'yes';
def.doresamplefs = 100;
def.ncomp       = 'auto';

cfg = setdef(cfg,def);

%% load the data
cfgselect = [];
cfgselect.channel = cfg.channel;
if ~exist('data','var')
    cfg.channel = 'all';
    
    dataorig        = ft_preprocessing(cfg);
else
    dataorig = data;
end
data            = ft_selectdata(cfgselect,dataorig);


%% exclude bad segments if required
if istrue(cfg.excludebad)
    cfgexclude = [];
    cfgexclude.markerfile = strrep(cfg.dataset,'.fif','.txt');
    cfgexclude.dataset = cfg.dataset;
    cfgexclude.excludename = 'BAD';
    data = ft_excludebad(cfgexclude,data);
end

%% downsample the data to speed up the next step
if istrue(cfg.doresample)
    cfgrsmp = [];
    cfgrsmp.resamplefs  = cfg.doresamplefs;
    cfgrsmp.detrend     = 'no';
    data      = ft_resampledata(cfgrsmp, data);
end

%% run the ICA
cfgica              = [];
cfgica.method       = 'runica';
if strcmp(cfg.ncomp,'auto')
    cfgica.numcomponent = round(rank(data.trial{1}(:,:))*2/3);
else
    cfgica.numcomponent = cfg.ncomp;
end
comp             = ft_componentanalysis(cfgica, data);

%% recompute the ica activation in the original data
% ignoring channels that have been excluded from the ICA
c = chnb(comp.topolabel,dataorig.label);
unmix = zeros(size(comp.unmixing,1),size(dataorig.trial{1},1));
unmix(:,c) = comp.unmixing;
comp.unmixing = unmix;
topo = zeros(size(dataorig.trial{1},1),size(comp.unmixing,1));
topo(c,:) = comp.topo;
comp.topo = topo;
comp.topolabel = dataorig.label;

comp.trial = {};comp.time = {};
comp.trial{1} = comp.unmixing * dataorig.trial{1};
comp.time = dataorig.time;
comp.fsample = dataorig.fsample;

data = dataorig;
clear dataorig


end
