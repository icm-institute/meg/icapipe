
file = 'example_data.fif';

cfg  = [];
cfg.dataset = file;
cfg.channel = {'MEG' 'BIO*'};
data = ft_preprocessing(cfg);


cfgrsmp = [];
cfgrsmp.resamplefs  = 100;
cfgrsmp.detrend     = 'no';
data      = ft_resampledata(cfgrsmp, data);

cfgica              = [];
cfgica.method       = 'runica';
cfgica.numcomponent = 50;
comp             = ft_componentanalysis(cfgica, data);

cfg                           = [];
cfg.component                 = [1 10];
data_corrected                = ft_rejectcomponent(cfg, comp, data );
